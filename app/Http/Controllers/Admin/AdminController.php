<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\CommonController;
use App\Models\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use \PDF;
use Validator;
use App\Models\OrderActionStatus;
use App\Models\Orders;
use CreateOrderActionStatus;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->common_class_obj = new CommonController();
    }

    //
    public function index()
    {
        $totalCompletedOrders = Orders::where([['order_status', '=', 5], ['active_status', '=', 1]])->count();
        $totalRunningOrders = Orders::where('active_status', '=', 1)->whereIn('order_status', [1,2,3])->count();
        $totalNewOrders = Orders::where([['order_status', '=', 0], ['active_status', '=', 1]])->count();
        
        $runningOrderList = Orders::where([['active_status', '!=', 0]])->orderBy('id', 'DESC')->limit(5)->get();
        $deliveryMans = User::where([['role_id', '=', 2], ['status', '=', 1]])->get();
        //dd($runningOrderList);
        return view('home',compact('totalCompletedOrders','totalRunningOrders','totalNewOrders','runningOrderList','deliveryMans'));
    }

    public function list()
    {
        $adminList = User::where([['role_id', '=', 1], ['status', '=', 1]])->get();
        
        
        return view('admin.list',compact('adminList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {
        
        if(strlen($request->name) < 5 ||  strlen($request->name) >30){
            return redirect()->route('admin.list')->with('error', 'Name should be at least 5 character and maximum 30 characters');
        }

        if(strlen($request->phone_no) != 11 ){
            return redirect()->route('admin.list')->with('error', 'Phone Number should be 11 digits');
        }

        if (!filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
            return redirect()->route('admin.list')->with('error', 'Invalid Email Format');
        }

        if(empty($request->address) ){
            return redirect()->route('admin.list')->with('error', 'Address Should not be empty');
        }
        

        if (User::where([['email', '=', $request->email],['status','=',1]])->first()) {
            return redirect()->route('admin.list')->with('error', 'This Email  ' . $request->input('email') . ' Already Used');
         }
        
         $defaultPassword = "12345678";

        $admin = new User();
        $admin->name = $request->name;
        $admin->phone_no = $request->phone_no;
        $admin->email = $request->email;
        $admin->address =  empty($request->address) ? "" : $request->address;
        $admin->role_id =  1;
        $admin->password = Hash::make($defaultPassword);

        

        $admin->save();

        if ($admin) {
            return redirect()->route('admin.list')->with('success', 'Admin ' . $request->input('name') . ' Added Successfully');
        }
    }

    public function updateData(Request $request)
    {
       
        if(strlen($request->name) < 5 ||  strlen($request->name) >30){
            return redirect()->route('admin.list')->with('error', 'Name should be at least 5 character and maximum 30 characters');
        }

        if(strlen($request->phone_no) != 11 ){
            return redirect()->route('admin.list')->with('error', 'Phone Number should be 11 digits');
        }

        if (!filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
            return redirect()->route('admin.list')->with('error', 'Invalid Email Format');
        }

        if(empty($request->address) ){
            return redirect()->route('admin.list')->with('error', 'Address Should not be empty');
        }
        

        if (User::where([['email', '=', $request->email],['id','!=', $request->id],['status','=',1]])->first()) {
            return redirect()->route('admin.list')->with('error', 'This Email  ' . $request->input('email') . ' Already Used');
         }
        
        $admin = User::findorFail($request->id);
        
        $admin->name = $request->name;
        $admin->phone_no = $request->phone_no;
        $admin->email = $request->email;
        $admin->address =  empty($request->address) ? "" : $request->address;
        

        

        $admin->save();

        if ($admin) {
            return redirect()->route('admin.list')->with('success', 'Admin ' . $request->input('name') . ' Upadated Successfully');
        }
    }


    public function deleteAdmin($id)
    {
        
        $admin = User::findorFail($id);
        
        $admin->status = 0;

        $admin->save();

        return redirect()->route('admin.list')->with('success', 'Admin ' . $admin->name . ' Deleted Successfully');
    }

    // public function details($id)
    // {
    //     dd("details");
    //     $deliveryMan = User::findorFail($id);
    //     return view('deliveryMan.details', compact('deliveryMan', 'id'));
    // }
}
