<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Customer\CustomerController;
use App\Http\Controllers\Customer\CustomerOrderController;
use App\Http\Controllers\DeliveryMan\DeliveryManController;
use App\Http\Controllers\Auth\LoginController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();

// Route::post('/main/checklogin', [LoginController::class,'checklogin']);
// Route::get('/redirect', [LoginController::class,'redirectTo']);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/change/password', [App\Http\Controllers\Auth\LoginController::class, 'changePasswordUI'])->name('changePasswordUI');
Route::post('/change-password', [App\Http\Controllers\Auth\LoginController::class, 'changePasswordConfirm'])->name('changePasswordConfirm');

/// Admin
Route::get('/adminDashboard', [AdminController::class,'index'])->name('adminDashboard')->middleware('admin');
Route::get('/admin-list', [AdminController::class,'list'])->name('admin.list')->middleware('admin');
Route::post('/admin/add/', [AdminController::class,'add'])->name('admin.add')->middleware('admin');
Route::post('/admin/updateData/', [AdminController::class,'updateData'])->name('admin.updateData')->middleware('admin'); 
Route::delete('/deleteAdmin/{id}', [AdminController::class,'deleteAdmin'])->name('admin.deleteAdmin')->middleware('admin');
//Route::get('/details/{id}', [AdminController::class,'details'])->name('admin.details')->middleware('admin');
Route::get('/admin/order-list/', [CustomerOrderController::class,'getDeliveryOrderForAdmin'])->name('admin.getDeliveryOrderForAdmin')->middleware('admin');
Route::post('/admin/order-reject/', [CustomerOrderController::class,'orderReject'])->name('order.order-reject')->middleware('admin');
Route::post('/admin/assign-delivery-man/', [CustomerOrderController::class,'assignDeliveryMan'])->name('order.order-assign-delivery-man')->middleware('admin');
Route::get('/admin/order-history/', [CustomerOrderController::class,'adminOrderHistory'])->name('admin.adminOrderHistory')->middleware('admin');

/// Customer
Route::get('/customerDashboard', [CustomerController::class,'index'])->name('customerDashboard')->middleware('customer');
Route::get('/customer-list', [CustomerController::class,'list'])->name('customer.list')->middleware('admin');
Route::get('/details-customer/{id}', [CustomerController::class,'detailsCustomer'])->name('customer.detailsCustomer')->middleware('admin');

Route::resource('customer-order',CustomerOrderController::class)->middleware('customer');;
Route::get('/customer/new-order', [CustomerOrderController::class,'newOrderRequest'])->name('customer-order.newOrderRequest')->middleware('customer');
Route::get('/customer/running-order/', [CustomerOrderController::class,'runnngOrder'])->name('customer-order.runnngOrder')->middleware('customer');
Route::get('/customer/order-history/', [CustomerOrderController::class,'orderHistory'])->name('customer-order.orderHistory')->middleware('customer');

/// Delivery Man
Route::get('/deliveryManDashboard', [DeliveryManController::class,'index'])->name('deliveryManDashboard')->middleware('deliveryMan');
Route::get('/delivery-man-list', [DeliveryManController::class,'list'])->name('delivery-man.list')->middleware('admin');
Route::post('/delivery-man/add/', [DeliveryManController::class,'add'])->name('delivery-man.add')->middleware('admin');
Route::post('/delivery-man/updateData/', [DeliveryManController::class,'updateData'])->name('delivery-man.updateData')->middleware('admin'); 
Route::delete('/deleteDeliveryMan/{id}', [DeliveryManController::class,'delete'])->name('delivery-man.delete')->middleware('admin');
Route::get('/detailsDeliveryMan/{id}', [DeliveryManController::class,'details'])->name('delivery-man.details')->middleware('admin');
Route::get('/delivery-man/assign-order-list/', [CustomerOrderController::class,'assignOrderList'])->name('order.assign-order-list')->middleware('deliveryMan');
Route::get('/delivery-man/completed/', [CustomerOrderController::class,'deliveryManCompleted'])->name('order.delivery-man-completed')->middleware('deliveryMan');

Route::get('/order-details/{id}', [CustomerOrderController::class,'orderDetails'])->name('order.order-details')->middleware('deliveryMan');  // 'deliveryMan','admin', customer -- here Data :p 
Route::get('/order-details-admin/{id}', [CustomerOrderController::class,'orderDetails'])->name('order.order-details-admin')->middleware('admin');  // 'deliveryMan','admin', customer
Route::get('/order-details-customer/{id}', [CustomerOrderController::class,'orderDetails'])->name('order.order-details-customer')->middleware('customer');  // 'deliveryMan','admin', customer

Route::post('/assign-order-received', [CustomerOrderController::class,'assignOrderReceived'])->name('order.assign-order-received')->middleware('deliveryMan');
Route::post('/assign-order-add-location', [CustomerOrderController::class,'assignOrderAddLocation'])->name('order.assign-order-add-location')->middleware('deliveryMan');
Route::post('/assign-order-complete', [CustomerOrderController::class,'assignOrderComplete'])->name('order.assign-order-complete')->middleware('deliveryMan');
Route::get('/map/{id}', [CustomerOrderController::class,'mapView'])->name('order.map'); //->middleware('deliveryMan');

