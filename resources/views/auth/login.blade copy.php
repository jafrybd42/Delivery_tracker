@extends('layouts.app')


@section('extra-css')
    <link href="https://fonts.googleapis.com/css?family=Poppins:600&display=swap" rel="stylesheet">
    <link href="{{ asset('../admin/css/loginForm.css') }}" rel="stylesheet">

@endsection
@section('content')

    <img class="wave" src="{{ asset('../admin/img/wave.png') }}">
    <div class="container">
        <div class="img">
            <img src="{{ asset('../admin/img/deliver2.png') }}">
        </div>
        @if ($errors->any())
                    <div class="col-sm-12">
                        <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                            @foreach ($errors->all() as $error)
                            <span>
                                <p>{{ $error }}</p>
                            </span>
                            @endforeach
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    @endif
            
                    @include('flash-message')
        <div class="login-content">
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <!-- <img src="https://raw.githubusercontent.com/sefyudem/Responsive-Login-Form/master/img/avatar.svg"> -->
                <!-- <h2 class="title">Welcome</h2> -->
                <div class="input-div one">
                    <div class="i">
                        <i class="fas fa-user"></i>
                    </div>
                    <div class="div">
                        <h5>{{ __('E-Mail') }}</h5>
                        <input id="email" type="email" class="input @error('email') is-invalid @enderror" name="email"
                            value="{{ old('email') }}" required autocomplete="email" autofocus>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>
                </div>
                <div class="input-div pass">
                    <div class="i">
                        <i class="fas fa-lock"></i>
                    </div>
                    <div class="div">
                        <h5>{{ __('Password') }}</h5>
                        <input id="password" type="password" class="input @error('password') is-invalid @enderror"
                            name="password" required autocomplete="current-password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="input-div">
                    <div class="col-md-6 offset-md-4">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember"
                                {{ old('remember') ? 'checked' : '' }}>

                            <label class="form-check-label" for="remember">
                                {{ __('Remember Me') }}
                            </label>
                        </div>
                    </div>
                </div>

                <div class="input-div">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Login') }}
                    </button>

                    @if (Route::has('password.request'))
                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            {{ __('Forgot Your Password?') }}
                        </a>
                    @endif

                    @if (Route::has('register'))
                        <a class="btn btn-link" href="{{ route('register') }}">{{ __('Register as a new user') }}</a>
                    @endif


                </div>

            </form>
        </div>
    </div>

@endsection

@section('extra-js')

    <script src="https://kit.fontawesome.com/a81368914c.js"></script>
    <script src="{{ asset('../admin/js/loginForm.js') }}"></script>

@endsection
