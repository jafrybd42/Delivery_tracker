@extends('layouts.app')

@section('content')
<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12">
                @if ($errors->any())
                <div class="col-sm-12">
                    <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                        @foreach ($errors->all() as $error)
                        <span>
                            <p>{{ $error }}</p>
                        </span>
                        @endforeach
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
                @endif

                @include('flash-message')
                <div class="card card-statistic-2">
                    <div class="card-icon shadow-primary bg-primary">
                        <i class="fas fa-archive"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Total Completed</h4>
                        </div>
                        <div class="card-body">
                            {{ $totalCompletedOrders }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="card card-statistic-2">
                    <div class="card-icon shadow-primary bg-primary">
                        <i class="fas fa-dollar-sign"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Total Running</h4>
                        </div>
                        <div class="card-body">
                            {{ $totalRunningOrders }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="card card-statistic-2">
                    <div class="card-icon shadow-primary bg-primary">
                        <i class="fas fa-shopping-bag"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>New Orders</h4>
                        </div>
                        <div class="card-body">
                            {{ $totalNewOrders }}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Delivery Requests</h4>
                        <div class="card-header-action">
                            <a href="{{ route('admin.getDeliveryOrderForAdmin') }}" class="btn btn-danger">View More <i class="fas fa-chevron-right"></i></a>
                        </div>
                    </div>
                    <div class="card-body p-0">
                        <div class="table-responsive table-invoice">
                            <table class="table table-striped">
                                <tr>
                                    <th>#</th>
                                    <th>Customer</th>
                                    <th>Delivery Man</th>
                                    <th>Product Category</th>
                                    <!-- <th>Weight</th> -->
                                    <th>Status</th>
                                    <th>Order Date</th>
                                    <th>Delivered To</th>
                                    <th>Contact Number</th>
                                    <th>Details</th>
                                    <th width="250px">Action</th>
                                </tr>
                                <tbody>
                                    @foreach ($runningOrderList as $key => $order)

                                    <?php
                                    $status = "";
                                    if ($order->order_status == -1) {
                                        $status = "Reject";
                                    } else if ($order->order_status == 0) {
                                        $status = "Pending";
                                    } else if ($order->order_status == 1) {
                                        $status = "Accept";
                                    } else if ($order->order_status == 2) {
                                        $status = "Receive";
                                    } else if ($order->order_status == 3) {
                                        $status = "On the way";
                                    } else if ($order->order_status == 5) {
                                        $status = "Delivered";
                                    }

                                    ?>
                                    <tr>
                                        <td>{{ $key + 1 }}</td>

                                        <td> <a href="{{ route('customer.detailsCustomer', $order->customer->id) }}"> {{ $order->customer->name }} </a> </td>

                                        @if($order->deliveryMan_id == null)
                                        <td>Not Assign</td>
                                        @else
                                        <td> <a href="{{ route('delivery-man.details', $order->deliveryMan->id) }}"> {{ $order->deliveryMan->name }} </a> </td>

                                        @endif

                                        <td>{{ $order->product_category }}</td>

                                        <!-- <td>{{ $order->product_weight }}</td> -->

                                        <td>{{ $status}}</td>


                                        <td>{{ \Carbon\Carbon::parse($order->created_at)->format('d/m/Y') }}</td>
                                        <td>{{ $order->delivered_to }}</td>
                                        <td>{{ $order->contact_number }}</td>

                                        <td>
                                            <a href="{{ route('order.order-details-admin', $order->id) }}" class="btn btn-primary"> <i class="far fa-eye"></i> </a>
                                        </td>

                                        <td>

                                            @if( $order->order_status == 0 && $order->active_status == 1)

                                            <button type="button" class="btn btn-primary" data-toggle="modal" onclick="changeModalData('{{ $order->id}}')" data-target="#modal-part">
                                                Accept
                                            </button>



                                            <form class="btn btn-danger" id="logout-form" action="{{ route('order.order-reject') }}" method="POST">
                                                @csrf
                                                <input type="hidden" name="id" value="{{ $order->id}}">
                                                <input type="submit" value="reject">
                                            </form>

                                            @endif
                                        </td>
                                    </tr>


                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modal-part">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Assign Delivery Man</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form action="{{ route('order.order-assign-delivery-man') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <ul class="list-group list-group-flush">

                        <li class="list-group-item form-group">
                            Select Delivery Man: <br>
                            <select class="form-control" name="delivery_man_id" id="delivery_man_id">
                                @foreach ($deliveryMans as $key => $man)
                                <option value="{{ $man->id }}">{{$man->name}} ( {{ $man->phone_no }})</option>
                                @endforeach
                            </select>

                            <input type="hidden" name="id" id="model_id" value="0">
                        </li>




                    </ul>
                </div>
                <div class="modal-footer bg-whitesmoke br">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">
                        Close
                    </button>
                    <input type="submit" class="btn btn-primary ml-2">


                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('extra-js')

<script>
    function changeModalData(id = 0) {
        document.getElementById('model_id').value = id;
    }
</script>

@endsection